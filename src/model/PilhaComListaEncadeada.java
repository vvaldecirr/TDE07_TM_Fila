package model;

public class PilhaComListaEncadeada<T> extends Pilha<T> {

	private Node topo;
	
	public PilhaComListaEncadeada() {
		
	}

	private Node getTopo() {
		return topo;
	}

	public void setTopo(Node topo) {
		this.topo = topo;
	}

	@Override
	boolean adicionarNoTopo(T elemento) {
		if (elemento == null)
			return false;
		
		Node novoNo = new Node((String) elemento);
		
		if (this.getTopo() != null)
			novoNo.setNoAnterior(topo);

		this.setTopo(novoNo);
		
		return false;
	}

	@Override
	T retirarDoTopo() {
		if (this.getTopo() == null)
			return null;
		
		Node elemento = (Node) this.getTopo();
				
		this.setTopo(this.getTopo().noAnterior);
		
		super.setNumElementos(super.getNumElementos()-1);		
		
		return (T) elemento.getValue();
	}

	@Override
	boolean ehPossivelAdicionar() {
		return true;
	}

	public class Node {
		private String value;
		private Node noAnterior;

		public Node(String value) {
			this.setValue(value);
		}

		public String getValue() {
			return this.value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public Node getNoAnterior() {
			return this.noAnterior;
		}

		public void setNoAnterior(Node noAnterior) {
			this.noAnterior = noAnterior;
		}
	}
	
}
