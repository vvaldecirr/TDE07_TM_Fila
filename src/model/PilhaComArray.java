package model;

public class PilhaComArray<T> extends Pilha<T> {
	
	private T[] pilha;
	private int topo;
	
	public PilhaComArray(int size) {
		this.pilha = (T[]) new String[size];
		this.setTopo(-1);
	}

	@Override
	public boolean adicionarNoTopo(T elemento) {
		
		if (this.getTopo() < 0)
			this.setTopo(0);
		else
			this.setTopo(this.getTopo()+1);

		this.pilha[this.getTopo()] = elemento;			
		
		return false;
	}

	@Override
	public T retirarDoTopo() {
		
		if (this.getTopo() < 0)
			return null;
		else {
			String elemento = (String) this.pilha[this.getTopo()];
			this.setTopo(this.getTopo()-1);
			
			return (T) elemento;
		}
	}

	@Override
	boolean ehPossivelAdicionar() {
		if (super.getNumElementos() < this.pilha.length)
			return true;
		else 
			return false;
	}

	public int getTopo() {
		return topo;
	}

	public void setTopo(int topo) {
		this.topo = topo;
	}

}
