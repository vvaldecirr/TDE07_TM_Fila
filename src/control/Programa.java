package control;

import model.PilhaComArray;
import model.PilhaComListaEncadeada;

/**
 * 
 * @author Valdecir
 * Duas possíveis implementações para a pilha
 *
 */
public class Programa {

	public static void main(String[] args) {
		
//		PilhaComArray<String> pilha = new PilhaComArray<String>(3);
		PilhaComListaEncadeada<String> pilha = new PilhaComListaEncadeada();
		
		pilha.push("Black");
		pilha.push("Nina");
		pilha.push("Chan");
		
		System.out.println("Total de elementos na pilha: "+pilha.getNumElementos());

		pilha.push("Silva");
		
		pilha.pop();

		System.out.println("Total de elementos na pilha: "+pilha.getNumElementos());
		
	}

}
